#pragma once
#include <Windows.h>
#include <iostream>
#include <vector>
#include <string>
#include "proc.h"
#include "helpers.h"
#include "Abstract/AbstractMemoryHandler.h"
#include "Impl/MemoryHandler.h"

enum class CodeInjectionType {
	NOP,
	HOOK,
	REPLACE
};

class CodeInjector
{
private:
	HANDLE Process;
	std::vector<char> OriginalCode;
	std::vector<char> Payload;
	uintptr_t TrampAddr;
	uintptr_t RetAddr;
	uintptr_t HookAddr;
	std::size_t OriginalSize;
	AbstractMemoryHandler* Memory;

	bool Allocate();
	bool CopyOriginal();
	bool CopyPayload();
	bool Free();
	bool Initialize();
public:
	bool Injected;
	bool Active;
	CodeInjectionType Type;

	uintptr_t& GetTrampAddress() { return TrampAddr; }
	uintptr_t& GetHookAddress() { return HookAddr; }
	uintptr_t& GetReturnAddress() { return RetAddr; }

	CodeInjector();

	bool Initialize(HANDLE targetProcess, uintptr_t address, CodeInjectionType type, std::size_t originalSize = -1);
	void SetPayload(std::vector<char> payload);

	~CodeInjector();

	bool SetTarget(HANDLE targetProcess);
	bool Reset();

	bool InsertCode();
	bool RestoreCode();
};
