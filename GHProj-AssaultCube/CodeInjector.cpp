#include "CodeInjector.h"

bool CodeInjector::Allocate()
{
	if (!IsValidProcess(Process)) return false;

	if (!Memory->Allocate(Process, &TrampAddr, Payload.size() + 0x24)) {
		return false;
	}

	return true;
}

bool CodeInjector::CopyOriginal()
{
	if (!IsValidProcess(Process)) return false;
	OriginalCode.reserve(OriginalSize);

	std::fill(OriginalCode.begin(), OriginalCode.end(), '\0');

	char* buffer = new char[OriginalSize];

	if (!Read(Process, HookAddr, OriginalSize, buffer)) {
		std::cout << "Failed to copy original code" << std::endl;
		return false;
	}

	std::copy(buffer, buffer + OriginalSize, OriginalCode.data());

	return true;
}

bool CodeInjector::CopyPayload()
{
	if (!IsValidProcess(Process))
		return false;

	if (Type != CodeInjectionType::HOOK) {
		Injected = true;
		return true;
	}
	if (Payload.size() == 0)
		return false;
	if (!Allocate())
		return false;

	auto payload = Payload;
	auto toAddr = HookAddr + OriginalCode.size() + 5;
	auto fromAddr = TrampAddr + Payload.size();
	auto jmpPayload = GenerateJump(fromAddr, toAddr);

	for (auto& c : jmpPayload) {
		payload.emplace_back(c);
	}

	if (!Write(Process, TrampAddr, payload.size(), payload.data())) {
		std::cout << "Failed to write payload" << std::endl;
		return false;
	}
	
	Injected = true;
	return true;
}

bool CodeInjector::Free()
{
	if (!IsValidProcess(Process)) return false;

	if (TrampAddr == 0)
		return false;

	RestoreCode();

	if (!Memory->Free(Process, TrampAddr, Payload.size() + 0x24)) {
		std::cout << "[Ex] Failed to allocate memory: " << std::hex << GetLastError() << std::endl;
		return false;
	}

	return true;
}

bool CodeInjector::Initialize()
{
	if (!CopyOriginal()) 
		return false;
	return true;
}

CodeInjector::CodeInjector() : 
	Process(INVALID_HANDLE_VALUE), 
	OriginalCode({}), 
	Payload({}), 
	TrampAddr(0), 
	RetAddr(0), 
	HookAddr(0), 
	OriginalSize(5),
	Active(false), 
	Injected(false),
	Memory(new MemoryHandler()),
	Type(CodeInjectionType::HOOK)
{}

bool CodeInjector::Initialize(HANDLE targetProcess, uintptr_t address, CodeInjectionType type, std::size_t originalSize)
{
	Type			= type;
	Process			= targetProcess;
	HookAddr		= address;

	if (Type != CodeInjectionType::HOOK) {
		if (originalSize == -1) {
			return false;
		}

		OriginalSize = originalSize;
	}
	else {
		OriginalSize = originalSize == -1 ? 5 : originalSize;
	}
	

	return Initialize();
}

void CodeInjector::SetPayload(std::vector<char> payload)
{
	if (Type != CodeInjectionType::HOOK) {
		if (Type == CodeInjectionType::NOP) {
			return;
		} else if (Payload.size() != OriginalSize) {
			throw std::exception("Wrong payload size must have the same size as Original code");
		}
		
		Payload = payload;
		return;
	}

	if (Injected) {
		if (Payload.size() < payload.size()) {
			if (!Free()) {
				throw std::exception("Failed to free");
			}

			if (!Allocate()) {
				throw std::exception("Failed to allocate");
			}
		}
	}

	Payload = payload;

	if (!CopyPayload()) {
		throw std::exception("Failed to insert payload");
	}
}

CodeInjector::~CodeInjector()
{
	if (IsValidProcess(Process)) {
		RestoreCode();
		Free();
	}

	Injected = false;
	Active = false;
}

bool CodeInjector::SetTarget(HANDLE targetProcess)
{
	if (targetProcess == INVALID_HANDLE_VALUE) {
		std::cout << "[Ex] Invalid handle value" << std::endl;
		return false;
	}

	DWORD exitCode = STILL_ACTIVE;
	if (!GetExitCodeProcess(Process, &exitCode) || exitCode != STILL_ACTIVE) {
		std::cout << "[Ex] Dead process" << std::endl;
		return false;
	}

	Injected = false;
	Active = false;

	if (IsValidProcess(Process)) {
		RestoreCode();
		Free();
	}

	Process = targetProcess;

	if (!Initialize()) return false;

	return true;
}

bool CodeInjector::Reset()
{
	if (!IsValidProcess(Process)) return false;

	if (!RestoreCode()) return false;
	Active = false;

	if (!Free()) return false;
	Injected = false;
	
	if (!Initialize()) return false;

	Injected = true;

	return false;
}

bool CodeInjector::InsertCode()
{
	if (!IsValidProcess(Process)) 
		return false;
	if (Active)
		return true;

	char* ReadOutput = new char[5];

	if (Type != CodeInjectionType::HOOK) {
		std::vector<char> buffer;

		if (Type == CodeInjectionType::NOP) {
			std::vector<char> nopBuffer(OriginalSize);

			std::fill(nopBuffer.begin(), nopBuffer.end(), '\x90');

			buffer = nopBuffer;
		}
		else {
			buffer = Payload;
		}

		if (!Write(Process, HookAddr, buffer.size(), buffer.data())) {
			Active = false;
			return false;
		}

		Active = true;
		return true;
	}

	if (!Read(Process, HookAddr, 5, ReadOutput)) {
		std::cout << "Unable to inject" << std::endl;
		return false;
	}

	if (ReadOutput[0] == 0xE9) {
		if (!Active) {
			std::cout << "Already injected by another source" << std::endl;
			return false;
		}
		return true;
	}

	auto payload = GenerateJump(HookAddr, TrampAddr);

	if (OriginalSize > 5) {
		for (auto i = 0; i < OriginalSize - 5; i++) {
			payload.emplace_back('\x90');
		}
	}

	if (!Write(Process, HookAddr, payload.size(), payload.data())) {
		std::cout << "Unable to inject code" << std::endl;
		return false;
	}

	Active = true;
	return true;
}

bool CodeInjector::RestoreCode()
{
	if (!IsValidProcess(Process)) return false;
	if (!Active) return true;

	if (!Write(Process, HookAddr, 5, OriginalCode.data())) {
		std::cout << "Unable to restore original code" << std::endl;
		return false;
	}

	Active = false;
	return true;
}
