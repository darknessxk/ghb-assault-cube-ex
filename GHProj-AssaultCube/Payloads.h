#pragma once
#include <vector>

std::vector<char> GetNoRecoilCode() {
	return std::vector<char>{ '\x58', '\x59', '\x8B', '\x46', '\x08'};
}

std::vector<char> GetAimCheckCode(std::vector<char> isVisibleAddr) {
	return std::vector<char>{
		/* test edi', edi */
		'\x85', '\xFF',
			/* jz -> payload end */
			'\x74', '\x12',
			/* mov */
			'\xC7', '\x05',
			/* isVisibleAddr */
			isVisibleAddr[0], isVisibleAddr[1], isVisibleAddr[2], isVisibleAddr[3],
			/* 0x00000001 */
			'\x01', '\x00', '\x00', '\x00',
			/* lea edi */
			'\x8D', '\x87',
			/* + 225 */
			'\x25', '\x02', '\x00', '\x00',
			/* jmp -> payload end */
			'\xEB', '\x0A',
			/* mov */
			'\xC7', '\x05',
			/* isVisibleAddr */
			isVisibleAddr[0], isVisibleAddr[1], isVisibleAddr[2], isVisibleAddr[3],
			/* 0x00000000 */
			'\x00', '\x00', '\x00', '\x00'
	};
}

std::vector<char> GetDamageFunctionCode() {
	return std::vector<char>{
			/* mov eax, 50F4F4 */
		'\xA1', '\xF4', '\xF4', '\x50', '\x00', 
			/* add eax, F8 */
			'\x05', '\xF8', '\x00', '\x00', '\x00', 
			/* add ebx, 04 */
			'\x83', '\xC3', '\x04', 
			/* cmp eax, ebx */
			'\x39', '\xD8', 
			/* jz -> payload end */
			'\x74', '\x07',
			// Original Code
			/* sub [ebx], edi */
			'\x29', '\x3B',
			/* sub ebx, 04 */
			'\x83', '\xEB', '\x04', 
			/* mov eax, edi */
			'\x8B', '\xC7',
	};
}