#include "AobReader.h"

std::vector<char> AobReader::Read(AobReaderParams params, bool* completed)
{
	if (completed == nullptr) return std::vector<char>();
	if (CurrentAddress == -1) {
		CurrentAddress = params.start_address;
		std::memset(Buffer, 0, BufferLen);
	} else if (CurrentAddress >= params.end_address) {
		*completed = true;
	}
	else {
		const auto diff = params.end_address - params.start_address;
		CurrentAddress += diff > BufferLen ? diff : BufferLen;
	}

	if (::Read(params.process, CurrentAddress, BufferLen, Buffer)) {
		return std::vector<char>(Buffer, Buffer + BufferLen);
	}

	*completed = true;
	return std::vector<char>();
}
