#pragma once
#include "../../Abstract/AbstractAobMethod.h"

class AobMethod : public AbstractAobMethod {
	virtual bool Find(std::vector<char> buffer, AobMethodParams params, uintptr_t* foundAddress) override;
};