#pragma once
#pragma once
#include "../../Abstract/AbstractAobReader.h"
#include "../../helpers.h"
#include "../../nt_structs.h"
#include "../../nt_functions.h"
#include "../../proc.h"

class AobReader : public AbstractAobReader {
private:
	intptr_t CurrentAddress = -1;
	char* Buffer;
	const int BufferLen = 512;
public:
	AobReader() : Buffer(new char[BufferLen]) {}
	~AobReader() { delete[] Buffer; }

	virtual std::vector<char> Read(AobReaderParams params, bool* completed) override;
};