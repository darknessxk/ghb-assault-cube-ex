#include "AobMethod.h"

bool AobMethod::Find(std::vector<char> buffer, AobMethodParams params, uintptr_t* foundAddress)
{
	const auto pattern = params.pattern;
	const auto mask = params.mask;

	if (
		// buffer can't be empty
		buffer.size() == 0 ||
		// pattern can't be empty
		pattern.size() == 0 ||
		// mask can't be empty
		mask.size() == 0 ||
		// mask must have the same size as pattern
		mask.size() != pattern.size() ||
		// pattern must have at least the same size
		buffer.size() < pattern.size()
		) {
		return false;
	}

	for (auto i = 0; i < buffer.size(); i++) {
		if (buffer[i] == params.pattern[i]) {
			auto copyIndex = i + 1;
			for (auto x = 0; x < pattern.size(); x++, copyIndex++) {
				if (mask[x] == '?' || mask[x] == '*') continue;
				if (buffer[copyIndex] != pattern[x]) return false;
			}
		}
	}

	return true;
}
