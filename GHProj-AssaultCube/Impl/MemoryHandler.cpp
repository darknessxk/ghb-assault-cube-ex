#include "MemoryHandler.h"

bool MemoryHandler::Allocate(HANDLE process, uintptr_t* address, uintptr_t size)
{
	if (!IsValidProcess(process)) return false;

	*address = (uintptr_t)VirtualAllocEx(process, 0, size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	return *address != 0;
}

bool MemoryHandler::Free(HANDLE process, uintptr_t address, uintptr_t size)
{
	if (!IsValidProcess(process)) return false;

	return VirtualFreeEx(process, (LPVOID)address, 0, MEM_RELEASE);
}
