#pragma once
#include "../Abstract/AbstractMemoryHandler.h"
#include "../helpers.h"

class MemoryHandler : public AbstractMemoryHandler {
	virtual bool Allocate(HANDLE process, uintptr_t* address, uintptr_t size) override;
	virtual bool Free(HANDLE process, uintptr_t address, uintptr_t size) override;
};