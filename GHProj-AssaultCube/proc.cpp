#include "proc.h"

uintptr_t GetModuleBaseAddress(DWORD procId, const std::wstring& modName)
{
	uintptr_t modBaseAddr = 0;
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, procId);
	if (hSnap != INVALID_HANDLE_VALUE)
	{
		MODULEENTRY32 modEntry;
		modEntry.dwSize = sizeof(modEntry);
		if (Module32First(hSnap, &modEntry))
		{
			do
			{
				if (!_wcsicmp(modEntry.szModule, modName.c_str()))
				{
					modBaseAddr = (uintptr_t)modEntry.modBaseAddr;
					break;
				}
			} while (Module32Next(hSnap, &modEntry));
		}
	}
	CloseHandle(hSnap);
	return modBaseAddr;
}

auto FindProcessByName(std::string name) -> DWORD
{
	HANDLE currp = INVALID_HANDLE_VALUE;
	HANDLE oldp = INVALID_HANDLE_VALUE;

	auto NtGetNextProcess = GetRoutineAddress<nt::functions::_NtGetNextProcess>("NtGetNextProcess", "ntdll.dll");

	auto completedTask = [](HANDLE currp, HANDLE oldp, BOOL fullClear = false) -> void {
		if (!fullClear) {
			if (oldp == currp)
				return;
			if (oldp == INVALID_HANDLE_VALUE)
				return;
			CloseHandle(oldp);
		}

		if (oldp != INVALID_HANDLE_VALUE && oldp == currp) {
			CloseHandle(oldp);
			return;
		}

		if (oldp != INVALID_HANDLE_VALUE)
			CloseHandle(oldp);
	};

	if (NtGetNextProcess == nullptr) {
		return -1;
	}

	auto hr = NtGetNextProcess(currp, MAXIMUM_ALLOWED, 0, 0, &currp);

	if (hr != 0) {
		return -1;
	}

	const auto retries = 4;
	auto currentAttempt = 0;

	do {
		if (oldp != INVALID_HANDLE_VALUE && oldp != currp) {
			CloseHandle(oldp);
			oldp = INVALID_HANDLE_VALUE;
		}

		char buf[1024] = { 0 };
		GetModuleFileNameExA(currp, 0, buf, MAX_PATH);
		if (strstr(buf, name.c_str())) {
			ZeroMemory(buf, 1024);
			auto pid = GetProcessId(currp);
			completedTask(currp, oldp);
			return pid;
		}

		oldp = currp;

		hr = NtGetNextProcess(currp, MAXIMUM_ALLOWED, 0, 0, &currp);

		if (hr == 0x8000001A && currentAttempt < retries) {
			hr = NtGetNextProcess(currp, MAXIMUM_ALLOWED, 0, 0, &currp); // second attempt?
			currentAttempt++;
		}
	} 
	while (hr == 0);

	completedTask(currp, oldp, true);
	return -1;
}
