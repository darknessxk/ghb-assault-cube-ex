#include "Hotkey.h"

bool Hotkey::Pressed() {
	return GetAsyncKeyState(VirtualKey) & 0x8000;
}

Hotkey::Hotkey(std::string name, __int32 vk, bool toggle): 
	Name(name), VirtualKey(vk), Toggle(toggle), 
	isRunning(true), run(false), IsActive(false), 
	toggled(false), KeyName() 
{
	
	switch (vk) {
	case VK_XBUTTON1:
		KeyName = "Mouse XB1";
		break;
	case VK_XBUTTON2:
		KeyName = "Mouse XB2";
		break;
	default:
		char buffer[32] = { 0 };
		auto scan = MapVirtualKey(vk, MAPVK_VK_TO_VSC) << 16;
		auto outputSize = GetKeyNameTextA(scan, buffer, 32);

		if (outputSize < 1) {
			auto lastError = GetLastError();
			throw std::exception("Invalid key");
			return;
		}
		KeyName = std::string(buffer, outputSize);
	}

}

void Hotkey::Reset() {
	isRunning = false;

	IsActive = false;
}

bool Hotkey::Active() {
	auto beingPressed = Pressed();

	if (Toggle) {
		if (beingPressed && !toggled) {
			toggled = true;
			IsActive = !IsActive;
		}
		else {
			toggled = false;
		}
	}
	else {
		IsActive = beingPressed;
	}

	return IsActive;
}
