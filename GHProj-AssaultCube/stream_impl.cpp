#include "stream_impl.h"

std::ostream& operator<<(std::ostream& os, const Weapon& curWeap)
{
	os << curWeap.Name << " (" << curWeap.Current << "/" << curWeap.Reserve << ")";
	return os;
}

std::ostream& operator<<(std::ostream& os, const Vector3& vec)
{
	os << vec.X << ", " << vec.Y << ", " << vec.Z;
	return os;
}

std::ostream& operator<<(std::ostream& os, const Vector2& vec)
{
	os << vec.X << ", " << vec.Y;
	return os;
}

std::ostream& operator<<(std::ostream& os, const Player& player)
{
	os << "Name: \"" << player.Name << "\"" << std::endl;
	os << "Health: " << player.Health << std::endl;
	os << "Armor: " << player.Armor << std::endl;
	os << "Position: " << player.Position << std::endl;
	os << "Is Standing: " << player.IsStanding << std::endl;
	os << "ViewAngles: " << player.ViewAngles << std::endl;
	os << "Player State[0]: " << player.PlayerState << std::endl;
	os << "Player State[1]: " << player.PlayerState2 << std::endl;
	os << "Current Weapon: " << player.CurrentWeapon << std::endl;

	return os;
}

std::ostream& operator<<(std::ostream& os, const Hotkey& hk)
{
	os << "[+ | " << hk.KeyName << "] " << hk.Name << ": " << (hk.IsActive ? "enabled" : "disabled") << std::endl;
	return os;
}
