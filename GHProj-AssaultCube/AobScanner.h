#pragma once
#include "proc.h"
#include <Windows.h>
#include <functional>
#include "Abstract/AbstractAobMethod.h"
#include "Abstract/AbstractAobReader.h"

class AobScanner
{
private:
	AbstractAobMethod* Method = nullptr;
	AbstractAobReader* Reader = nullptr;
public:
	AobScanner() {}
	AobScanner(AbstractAobMethod* method) : Method(method) {}
	AobScanner(AbstractAobReader* reader) : Reader(reader) {}
	AobScanner(AbstractAobMethod* method, AbstractAobReader* reader) : Method(method), Reader(reader) {}

	void setReader(AbstractAobReader* reader) {
		Reader = reader;
	}

	void setParser(AbstractAobMethod* method) {
		Method = method;
	}

	bool Search(AobReaderParams, AobMethodParams, uintptr_t* result);
};
