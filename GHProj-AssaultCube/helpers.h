#pragma once
#include <vector>
#include <array>
#include <charconv>
#include <type_traits>
#include <string>

#define NT_SUCCESS (NtStatus) (((NTSTATUS)(NtStatus)) >= 0)

template<typename T> concept AddressValidType = std::integral<T>;

template<int N>
__forceinline void byteswap_array(char(&bytes)[N]) {
	std::reverse(bytes, bytes + N);
}

template<typename T>
__forceinline T byteswap(T value) {
	byteswap_array(*reinterpret_cast<char(*)[sizeof(value)]>(&value));
	return value;
}

template<typename T> requires AddressValidType<T>
__forceinline std::vector<char> ConvertAddressToVector(T target) {
	target = byteswap(target);
	auto result = std::vector<char>(sizeof(T));
	
	for (auto index = 0; index < sizeof(T); index++) {
		result[3 - index] = (target >> (index * 8));
	}

	return result;
}

template<typename T, typename R = T> requires AddressValidType<T> and AddressValidType<R>
__forceinline R CalculateJump(T from, T to, std::size_t jumpSize) {
	constexpr T addrSize = sizeof(to);
	const auto asmSize = jumpSize + addrSize;
	const R ret = to - (from + asmSize);
	return ret;
}

template<typename T> requires AddressValidType<T>
__forceinline std::vector<char> GenerateJump(
	T from, 
	T to,
	std::vector<char> jumpPayload = { '\xE9' })
{
	intptr_t targetAddr = CalculateJump(from, to, jumpPayload.size());

	auto target = ConvertAddressToVector(targetAddr);

	jumpPayload.insert(jumpPayload.end(), target.begin(), target.end());

	return jumpPayload;
}

__forceinline bool IsValidProcess(HANDLE Process)
{
	if (Process == INVALID_HANDLE_VALUE) {
		return false;
	}

	DWORD exitCode = STILL_ACTIVE;
	if (!GetExitCodeProcess(Process, &exitCode) || exitCode != STILL_ACTIVE) {
		return false;
	}

	return true;
}

template <typename T>
auto GetRoutineAddress(std::string routine_name, std::string module_name) -> T
{
	HMODULE mod = GetModuleHandleA(module_name.c_str());
	if (mod) {
		T RoutineAddress = (T)GetProcAddress(mod, routine_name.c_str());
		if (RoutineAddress)
			return RoutineAddress;
		return nullptr;
	}
	return nullptr;
}
