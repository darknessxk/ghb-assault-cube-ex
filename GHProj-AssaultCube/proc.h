#pragma once
#include <Windows.h>
#include <Psapi.h>
#include <TlHelp32.h>
#include <string>
#include <vector>
#include <iostream>
#include "helpers.h"
#include "nt_functions.h"

uintptr_t GetModuleBaseAddress(DWORD procId, const std::wstring& modName);

auto FindProcessByName(std::string) -> DWORD;

template<typename T, typename H = HANDLE>
auto Read(H handle, uintptr_t address, std::size_t len, T* data) -> bool;

template<typename H = HANDLE>
auto ReadAddressFromOffset(H handle, uintptr_t* address, std::vector<uintptr_t> offsets) -> bool;

template<typename T, typename H = HANDLE>
auto Write(H handle, uintptr_t address, std::size_t len, T* data) -> bool;

template<typename T, typename H>
auto Read(H handle, uintptr_t address, std::size_t len, T* data) -> bool
{
	if (address == 0) {
		std::cout << "[Ex] Invalid address" << std::endl;
		return false;
	}

	if (len == 0) {
		std::cout << "[Ex] Invalid length" << std::endl;
		return false;
	}

	if (data == nullptr) {
		std::cout << "[Ex] Invalid data pointer" << std::endl;
		return false;
	}

	SIZE_T retVal = 0;
	if (!ReadProcessMemory(handle, (LPCVOID)address, data, len, &retVal)) {
		std::cout << "[Ex] Read failed: " << std::hex << GetLastError() << " | " << retVal << std::endl;
		return false;
	}

	return true;
}

template<typename T, typename H>
auto Write(H handle, uintptr_t address, std::size_t len, T* data) -> bool
{
	if (address == 0) {
		std::cout << "[Ex] Invalid address" << std::endl;
		return false;
	}

	if (len == 0) {
		std::cout << "[Ex] Invalid length" << std::endl;
		return false;
	}

	if (data == nullptr) {
		std::cout << "[Ex] Invalid data pointer" << std::endl;
		return false;
	}

	SIZE_T retVal = 0;
	if (!WriteProcessMemory(handle, (LPVOID)address, data, len, &retVal)) {
		std::cout << "[Ex] Write failed: " << std::hex << GetLastError() << " | " << retVal << std::endl;
		return false;
	}

	return true;
}

template<typename H>
auto ReadAddressFromOffset(H handle, uintptr_t* address, std::vector<uintptr_t> offsets) -> bool
{
	for (auto& offset : offsets) {
		if (!Read(handle, *address, sizeof(uintptr_t), &*address)) {
			return false;
		}

		*address += offset;
	}

	return true;
}

auto FindProcessByName(std::string name) -> DWORD;
