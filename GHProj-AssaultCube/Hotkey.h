#pragma once
#include <Windows.h>
#include <string>
#include <iostream>
#include <thread>
#include <mutex>
#include <functional>

class Hotkey
{
private:
	bool run;
	bool toggled;
	bool isRunning;

	bool Pressed();
public:
	const std::string	Name;
	std::string			KeyName;
	const __int32		VirtualKey;
	const bool			Toggle;
	bool IsActive;

	Hotkey(std::string name, __int32 vk, bool toggle = false);

	void Reset();

	bool Active();

	template<bool RunOnce = false, typename Callable, typename ...T>
	void Tick(Callable&& func, T&&... args)
	{
		if (Active()) {
			if (Toggle) {
				if (RunOnce) {
					if (!run)
					{
						run = true;
						func(std::forward<T>(args)...);
					}
				}
				else func(std::forward<T>(args)...);
			}
			else func(std::forward<T>(args)...);
		}
		else {
			if (run) func(std::forward<T>(args)...);
			run = false;
		}
	}
};

