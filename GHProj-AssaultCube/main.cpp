#include <iostream>
#include <Windows.h>
#include <stdio.h>
#include <string>
#include <chrono>
#include <vector>
#include "proc.h"
#include "structures.h"
#include "stream_impl.h"
#include "CodeInjector.h"
#include "Hotkey.h"
#include "Impl/MemoryHandler.h"
#include "Payloads.h"

AbstractMemoryHandler* Memory = new MemoryHandler();

struct Initialization {
	HANDLE Process;
	uintptr_t BaseAddress;
};

CodeInjector NoRecoil;
CodeInjector InfiniteAmmo;
CodeInjector GodMode;
CodeInjector PlayerOnAimCheckHook;
uintptr_t IsPlayerOnAimAddress;

Hotkey* SetAmmo;
Hotkey* SetHealth;
Hotkey* SetArmor;
Hotkey* ToggleNoRecoil;
Hotkey* ToggleGodMode;
Hotkey* ToggleInfiniteAmmo;
Hotkey* ToggleVisibilityCheck;
Hotkey* TriggerBotKey;
Hotkey* SaveTeleportKey;
Hotkey* LoadTeleportKey;
Hotkey* NoClipKey;

Initialization Initialize(DWORD pid) {
	auto proc = OpenProcess(PROCESS_ALL_ACCESS, 0, pid);

	if (proc == INVALID_HANDLE_VALUE) {
		std::cout << "Failed to open process with desired permissions: " << GetLastError() << std::endl;
		return { INVALID_HANDLE_VALUE, 0 };
	}

	auto base = GetModuleBaseAddress(pid, L"ac_client.exe");

	if (base == 0) {
		std::cout << "Failed to find process base address: " << GetLastError() << std::endl;
		return { INVALID_HANDLE_VALUE, 0 };
	}

	return { proc, base };
}

struct CheatInitialize {
	std::size_t PlayerBase;
};

CheatInitialize InitializeCheat(Initialization init) {
	uintptr_t LocalPlayerAddr = init.BaseAddress + 0x00109B74;
	std::vector<uintptr_t> offset = { 0x0 };

	if (!ReadAddressFromOffset(init.Process, &LocalPlayerAddr, offset)) {
		std::cout << "Failed to fetch self player address" << std::endl;
		return { 0 };
	}

	if (!NoRecoil.Initialize(init.Process, init.BaseAddress + 0x6378E, CodeInjectionType::HOOK)) {
		std::cout << "Failed to Initialize NoRecoil Code Injection" << std::endl;
		return { 0 };
	}

	if (!PlayerOnAimCheckHook.Initialize(init.Process, init.BaseAddress + 0xADA4, CodeInjectionType::HOOK, 0x1B)) {
		std::cout << "Failed to Initialize VisibilityCheckHook Code Injection" << std::endl;
		return { 0 };
	}

	if (!GodMode.Initialize(init.Process, init.BaseAddress + 0x29D1F, CodeInjectionType::HOOK)) {
		std::cout << "Failed to Initialize GodMode Code Injection" << std::endl;
		return { 0 };
	}

	if (!InfiniteAmmo.Initialize(init.Process, init.BaseAddress + 0x637E9, CodeInjectionType::NOP, 2)) {
		std::cout << "Failed to Initialize GodMode Code Injection" << std::endl;
		return { 0 };
	}

	NoRecoil.SetPayload(GetNoRecoilCode());
	GodMode.SetPayload(GetDamageFunctionCode());

	/* todo: optimize asm code to write only a single bool flag to memory, but for now */
	/* we will use one entire int (4 bytes) */
	if (!Memory->Allocate(init.Process, &IsPlayerOnAimAddress, sizeof(__int32))) {
		return { 0 };
	}

	auto isPlayerOnAimAddr = ConvertAddressToVector(IsPlayerOnAimAddress);
	PlayerOnAimCheckHook.SetPayload(GetAimCheckCode(isPlayerOnAimAddr));

	if (!PlayerOnAimCheckHook.InsertCode()) {
		std::cout << "Failed to inject VisibilityCheckHook" << std::endl;
		return { 0 };
	}

	return { LocalPlayerAddr };
}

int main() {
	using std::chrono::high_resolution_clock;
	using std::chrono::duration_cast;
	using std::chrono::duration;
	using std::chrono::milliseconds;

	SetAmmo = new Hotkey("Set Ammo", VK_NUMPAD7);
	SetArmor = new Hotkey("Set Armor", VK_NUMPAD8);
	SetHealth = new Hotkey("Set Health", VK_NUMPAD9);
	SaveTeleportKey = new Hotkey("Save current position", VK_NUMPAD0);
	LoadTeleportKey = new Hotkey("Load last saved position", VK_NUMPAD1);
	NoClipKey = new Hotkey("Toggle NoClip (Spectator Camera)", VK_NUMPAD2, true);

	ToggleNoRecoil = new Hotkey("Toggle No Recoil", VK_NUMPAD4, true);
	ToggleGodMode = new Hotkey("Toggle God Mode", VK_NUMPAD5, true);
	ToggleInfiniteAmmo = new Hotkey("Toggle Infinite Ammo", VK_NUMPAD6, true);
	TriggerBotKey = new Hotkey("Triggerbot", VK_XBUTTON1);

	auto pid = FindProcessByName("ac_client.exe");
	bool errorOnce = false;

	if (pid == -1) {
		std::cout << "Failed to find process: " << GetLastError() << std::endl;
		return -1;
	}

	auto init = Initialize(pid);

	Player self{};
	CheatInitialize cheatInfo = { 0 };
	DWORD lastErrorCodeEx = 0;

	constexpr char PlayerStateSpectateOn = 5;
	constexpr char PlayerStateSpectateOff = 0;
	constexpr char PlayerStateInvisibleOn = 4;
	constexpr char PlayerStateInvisibleOff = 0;

	auto CodeInjectorTickRoutine = [](CodeInjector* injector) -> void {
		if ((injector->Injected || injector->Type != CodeInjectionType::HOOK) && !injector->Active) {
			if (!injector->InsertCode()) {
				std::cout << "Failed to inject" << std::endl;
			}
		}
		else {
			injector->RestoreCode();
		}
	};

	auto WriteMemoryTickRoutine = [](HANDLE Process, uintptr_t Address, auto Value, std::string failureMessage) -> void {
		if (Address == 0) return;
		if (!Write(Process, Address, sizeof(Value), &Value)) std::cout << failureMessage;
	};

	auto ReadMemoryTickRoutine = [](HANDLE Process, uintptr_t Address, auto* Value, std::string failureMessage) -> void {
		if (Address == 0) return;
		if (!Read(Process, Address, sizeof(Value), Value)) std::cout << failureMessage;
	};

	__int32 IsPlayerOnAim;
	Vector3 SavedPosition = {0};
	bool NoClipEnabled = false;

	while (1) {
		auto routineStart = high_resolution_clock::now();
		if (!GetExitCodeProcess(init.Process, &lastErrorCodeEx) || lastErrorCodeEx != STILL_ACTIVE) {
			if (!errorOnce) {
				std::cout << "Error: Process ac_client.exe not found trying to attach again please start your Assault Cube again..." << std::endl;
				errorOnce = true;
			}

			pid = FindProcessByName("ac_client.exe");

			if (pid == -1) {
				continue;
			}

			cheatInfo = { 0 };
			init = Initialize(pid);
			errorOnce = false;
		}

		if (cheatInfo.PlayerBase == 0) {
			cheatInfo = InitializeCheat(init);

			if (cheatInfo.PlayerBase == 0) {
				std::cout << "Failed to initialize cheat" << std::endl;
				return -1;
			}
		}

		if (!self.GetData(init.Process, cheatInfo.PlayerBase)) {
			std::cout << "Failed to initialize player data" << std::endl;
			return -1;
		}

		if (!Read<__int32>(init.Process, IsPlayerOnAimAddress, 4, &IsPlayerOnAim)) {
			std::cout << "Unable to read if there is someone or not" << std::endl;
		}

		system("cls");

		std::cout << "--------------------PLAYER INFO---------------------" << std::endl;

		std::cout << self << std::endl;
		std::cout << "Player on aim: " << (IsPlayerOnAim == 1 ? "YES" : "NO") << std::endl;
		std::cout << "Last Saved Position: " << SavedPosition << std::endl;

		std::cout << "----------------------HOTKEYS-----------------------" << std::endl;
		TriggerBotKey->Tick([](__int32 isOnAim) {
			if (isOnAim == 1) {
				mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
				Sleep(5); // delay before releasing mouse
				mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
			}
		}, IsPlayerOnAim);
		std::cout << *TriggerBotKey << std::endl;

		SetAmmo->Tick(WriteMemoryTickRoutine, init.Process, self.CurrentWeapon.Value.Current.Address.Address, 1000, "Failed to write Current Ammo");
		std::cout << *SetAmmo << std::endl;

		SetHealth->Tick(WriteMemoryTickRoutine, init.Process, self.Health.Address.Address, 1000, "Failed to write Player Health");
		std::cout << *SetHealth << std::endl;

		SetArmor->Tick(WriteMemoryTickRoutine, init.Process, self.Armor.Address.Address, 1000, "Failed to write Player Armor");
		std::cout << *SetArmor << std::endl;

		SaveTeleportKey->Tick([](Player self, Vector3* SavedPosition) {
			*SavedPosition = self.ModifiablePosition.Value;
		}, self, &SavedPosition);
		std::cout << *SaveTeleportKey << std::endl;

		LoadTeleportKey->Tick(WriteMemoryTickRoutine, init.Process, self.ModifiablePosition.Address.Address, SavedPosition, "Failed to write Player Armor");
		std::cout << *LoadTeleportKey << std::endl;

		ToggleNoRecoil->Tick<true>(CodeInjectorTickRoutine, &NoRecoil);
		std::cout << *ToggleNoRecoil << std::endl;

		ToggleGodMode->Tick<true>(CodeInjectorTickRoutine, &GodMode);
		std::cout << *ToggleGodMode << std::endl;

		ToggleInfiniteAmmo->Tick<true>(CodeInjectorTickRoutine, &InfiniteAmmo);
		std::cout << *ToggleInfiniteAmmo << std::endl;

		NoClipKey->Tick<true>([&WriteMemoryTickRoutine](HANDLE process, Player player, bool& Enabled) {
			if (Enabled) {
				WriteMemoryTickRoutine(process, player.PlayerState.Address.Address, PlayerStateSpectateOff, "Failed to update PlayerState[0]");
				WriteMemoryTickRoutine(process, player.PlayerState2.Address.Address, PlayerStateInvisibleOff, "Failed to update PlayerState[1]");
				Enabled = false;
			}
			else {
				WriteMemoryTickRoutine(process, player.PlayerState.Address.Address, PlayerStateSpectateOn, "Failed to update PlayerState[0]");
				WriteMemoryTickRoutine(process, player.PlayerState2.Address.Address, PlayerStateInvisibleOn, "Failed to update PlayerState[1]");
				Enabled = true;
			}
		}, init.Process, self, NoClipEnabled);
		std::cout << *NoClipKey << std::endl;

		auto routineEnd = high_resolution_clock::now();

		auto ms_int = duration_cast<milliseconds>(routineEnd - routineStart);
		std::cout << "Loop took " << ms_int.count() << "ms" << std::endl;

		Sleep(10);
	}
}
