#pragma once
#include <iostream>
#include "structures.h"
#include "Hotkey.h"

std::ostream& operator<<(std::ostream& os, const Weapon& curWeap);
std::ostream& operator<<(std::ostream& os, const Vector3& vec);
std::ostream& operator<<(std::ostream& os, const Vector2& vec);
template<typename T>
std::ostream& operator<<(std::ostream& os, const AddressStruct<T>& addrData);
std::ostream& operator<<(std::ostream& os, const Player& player);
std::ostream& operator<<(std::ostream& os, const Hotkey& hk);

template<typename T>
std::ostream& operator<<(std::ostream& os, const AddressStruct<T>& addrData)
{
	os << addrData.Value;
	return os;
}
