#pragma once

namespace nt::structs {
    typedef enum _MEMORY_INFORMATION_CLASS {
        MemoryBasicInformation,
        MemoryWorkingSetList,
        MemorySectionName
    } MEMORY_INFORMATION_CLASS;
}
