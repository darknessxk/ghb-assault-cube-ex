#pragma once
#include <string>
#include <vector>
#include "proc.h"

struct AddressInfo {
	uintptr_t Address{};
	uintptr_t Length{};
	std::vector<uintptr_t> Offsets;
};

// Requires better naming lol
template<typename T> struct AddressStruct {
	T Value;
	AddressInfo Address;

	AddressStruct(AddressInfo info) {
		Address = info;
	}

	AddressStruct<T> operator =(const AddressInfo& addr) {
		Address = addr;
		return *this;
	}

	AddressStruct<T> operator =(const T& data) {
		Value = data;
		return *this;
	}
};

struct Vector3 {
	float X;
	float Y;
	float Z;
};

struct Vector2 {
	float X;
	float Y;
};

struct Weapon {
	AddressStruct<std::string> Name{
		AddressInfo{ 0, sizeof(char) * 64, std::vector<uintptr_t>{ 0xC, 0x0 } }
	};

	AddressStruct<__int32> Current{
		AddressInfo{ 0, sizeof(__int32), std::vector<uintptr_t>{ 0x14, 0x0 } }
	};

	AddressStruct<__int32> Reserve{
		AddressInfo{ 0, sizeof(__int32), std::vector<uintptr_t>{ 0x10, 0x0 } }
	};
};

struct Player {
	AddressStruct<__int32> Health{
		AddressInfo{ 0, sizeof(__int32), std::vector<uintptr_t>{ 0xF8 } }
	};
	
	AddressStruct<__int32> Armor{
		AddressInfo{ 0, sizeof(__int32), std::vector<uintptr_t>{ 0xFC } }
	};

	AddressStruct<bool> IsStanding{
		AddressInfo{ 0, sizeof(bool), std::vector<uintptr_t>{ 0x70 } }
	};

	AddressStruct<BYTE> PlayerState{
		AddressInfo{ 0, sizeof(BYTE), std::vector<uintptr_t>{ 0x338 } }
	};

	AddressStruct<BYTE> PlayerState2{
		AddressInfo{ 0, sizeof(BYTE), std::vector<uintptr_t>{ 0x82 } }
	};

	AddressStruct<Vector3> Position{
		AddressInfo{ 0, sizeof(Vector3), std::vector<uintptr_t>{ 0x04 } }
	};

	AddressStruct<Vector3> ModifiablePosition{
		AddressInfo{ 0, sizeof(Vector3), std::vector<uintptr_t>{ 0x34 } }
	};
	
	AddressStruct<Vector2> ViewAngles{
		AddressInfo{ 0, sizeof(Vector2), std::vector<uintptr_t>{ 0x40 } }
	};
	
	AddressStruct<std::string> Name{
		AddressInfo{ 0, sizeof(char) * 64, std::vector<uintptr_t>{ 0x225 } }
	};

	AddressStruct<Weapon> CurrentWeapon{
		AddressInfo{0, sizeof(Weapon), std::vector<uintptr_t>{ 0x374 }}
	};

	bool GetUserInfo(HANDLE Process, uintptr_t PlayerBase) {
		const __int32 bufferSize = 0x256;
		char* buffer = new char[bufferSize];
		if (!Read(Process, PlayerBase, bufferSize, buffer)) {
			std::cout << "Failed to fetch self player data buffer" << std::endl;
			return false;
		}

		Health = (__int32)*(buffer + Health.Address.Offsets[0]);
		Health.Address.Address = PlayerBase + Health.Address.Offsets[0];
		
		Armor = (__int32)*(buffer + Armor.Address.Offsets[0]);
		Armor.Address.Address = PlayerBase + Armor.Address.Offsets[0];

		Position = *reinterpret_cast<Vector3*>(buffer + Position.Address.Offsets[0]);
		Position.Address.Address = PlayerBase + Position.Address.Offsets[0];

		ModifiablePosition = *reinterpret_cast<Vector3*>(buffer + ModifiablePosition.Address.Offsets[0]);
		ModifiablePosition.Address.Address = PlayerBase + ModifiablePosition.Address.Offsets[0];

		IsStanding = (bool)*(buffer + IsStanding.Address.Offsets[0]);
		IsStanding.Address.Address = PlayerBase + IsStanding.Address.Offsets[0];

		PlayerState = (char)*(buffer + PlayerState.Address.Offsets[0]);
		PlayerState.Address.Address = PlayerBase + PlayerState.Address.Offsets[0];

		PlayerState2 = (char)*(buffer + PlayerState2.Address.Offsets[0]);
		PlayerState2.Address.Address = PlayerBase + PlayerState2.Address.Offsets[0];

		ViewAngles = *reinterpret_cast<Vector2*>(buffer + ViewAngles.Address.Offsets[0]);
		ViewAngles.Address.Address = PlayerBase + ViewAngles.Address.Offsets[0];

		Name = std::string(buffer + Name.Address.Offsets[0]);
		Name.Address.Address = PlayerBase + Name.Address.Offsets[0];

		delete[] buffer;

		return true;
	}

	bool GetCurrentWeapon(HANDLE Process, uintptr_t PlayerBase) {
		CurrentWeapon.Address.Address = PlayerBase + CurrentWeapon.Address.Offsets[0];

		uintptr_t currentWeaponNameAddress = CurrentWeapon.Address.Address; {
			if (!ReadAddressFromOffset(Process, &currentWeaponNameAddress, CurrentWeapon.Value.Name.Address.Offsets)) {
				std::cout << "Failed to read offset for CurrentAmmo" << std::endl;
				return false;
			}

			CurrentWeapon.Value.Name.Address.Address = currentWeaponNameAddress;
		}

		// PlayerBase + 0x368 + 0x14 + 0x0
		uintptr_t currentWeaponAmmoAddress = CurrentWeapon.Address.Address; {
			if (!ReadAddressFromOffset(Process, &currentWeaponAmmoAddress, CurrentWeapon.Value.Current.Address.Offsets)) {
				std::cout << "Failed to read offset for CurrentAmmo" << std::endl;
				return false;
			}
			CurrentWeapon.Value.Current.Address.Address = currentWeaponAmmoAddress;
		}

		// PlayerBase + 0x368 + 0x10 + 0x0
		uintptr_t currentWeaponReserveAddress = CurrentWeapon.Address.Address; {
			if (!ReadAddressFromOffset(Process, &currentWeaponReserveAddress, CurrentWeapon.Value.Reserve.Address.Offsets)) {
				std::cout << "Failed to read offset for ReserveAmmo" << std::endl;
				return false;
			}

			CurrentWeapon.Value.Reserve.Address.Address = currentWeaponReserveAddress;
		}

		{
			{
				const __int32 bufferSize = 0x128;
				char* buffer = new char[bufferSize];
				if (!Read(Process, currentWeaponNameAddress, bufferSize, buffer)) {
					std::cout << "Failed to fetch current weapon name buffer" << std::endl;
					return false;
				}

				CurrentWeapon.Value.Name = std::string(buffer);
				delete[] buffer;
			}

			{
				const __int32 bufferSize = sizeof(__int32);
				char* buffer = new char[bufferSize];
				if (!Read(Process, currentWeaponAmmoAddress, bufferSize, buffer)) {
					std::cout << "Failed to fetch current weapon current ammo buffer" << std::endl;
					return false;
				}

				CurrentWeapon.Value.Current = (int)*(buffer);
				delete[] buffer;
			}

			{
				const __int32 bufferSize = sizeof(__int32);
				char* buffer = new char[bufferSize];
				if (!Read(Process, currentWeaponReserveAddress, bufferSize, buffer)) {
					std::cout << "Failed to fetch current weapon reserve ammo buffer" << std::endl;
					return false;
				}

				CurrentWeapon.Value.Reserve = (int)*(buffer);
				delete[] buffer;
			}
		}
		return true;
	}

	bool GetData(HANDLE Process, uintptr_t PlayerBase) {
		if (!GetUserInfo(Process, PlayerBase)) return false;
		if (!GetCurrentWeapon(Process, PlayerBase)) return false;
		return true;
	}
};
