#include "AobScanner.h"

bool AobScanner::Search(AobReaderParams readerParams, AobMethodParams methodParams, uintptr_t* resultAddress)
{
	if (Reader == nullptr) return false;
	if (Method == nullptr) return false;
	bool completed = false;

	while (!completed) {
		auto buffer = Reader->Read(readerParams, &completed);

		if (Method->Find(buffer, methodParams, resultAddress)) {
			return true;
		}
	}

	return false;
}
