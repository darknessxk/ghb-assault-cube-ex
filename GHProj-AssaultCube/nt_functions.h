#pragma once
#include "nt_structs.h"
#include <Windows.h>

namespace nt::functions {
	typedef LONG(WINAPI* _ZwQueryVirtualMemory)(
		HANDLE ProcessHandle,
		PVOID BaseAddress,
		structs::MEMORY_INFORMATION_CLASS MemoryInformationClass,
		PVOID MemoryInformation,
		ULONG MemoryInformationLength,
		PULONG ReturnLength
	);

	typedef NTSTATUS(NTAPI* _NtGetNextProcess)(
		_In_ HANDLE ProcessHandle,
		_In_ ACCESS_MASK DesiredAccess,
		_In_ ULONG HandleAttributes,
		_In_ ULONG Flags,
		_Out_ PHANDLE NewProcessHandle
	);
}
