#pragma once
#include <vector>
#include <string>

struct AobMethodParams {
	std::vector<char> pattern;
	std::vector<char> mask;
};

class AbstractAobMethod {
public:
	virtual bool Find(std::vector<char> buffer, AobMethodParams params, uintptr_t* foundAddress) = 0;
};
