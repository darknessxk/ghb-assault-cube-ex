#pragma once
#include <Windows.h>

class AbstractMemoryHandler {
public:
	virtual bool Allocate(HANDLE process, uintptr_t* address, uintptr_t size) = 0;
	virtual bool Free(HANDLE process, uintptr_t address, uintptr_t size) = 0;
};