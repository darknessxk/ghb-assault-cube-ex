#pragma once
#include <vector>
#include <Windows.h>

struct AobReaderParams {
	HANDLE process;
	std::size_t start_address;
	std::size_t end_address;
};

class AbstractAobReader {
public:
	virtual std::vector<char> Read(AobReaderParams params, bool* completed) = 0;
};